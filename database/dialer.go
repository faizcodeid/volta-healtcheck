package database

import (
	"context"
	"database/sql/driver"
	"net"
	"time"

	"github.com/lib/pq"
	"golang.org/x/crypto/ssh"
)

type ViaSSHDialer struct {
	Client *ssh.Client
}

func (sshDial *ViaSSHDialer) Open(dsn string) (_ driver.Conn, err error) {
	return pq.DialOpen(sshDial, dsn)
}

func (sshDial *ViaSSHDialer) Dial(network, address string) (net.Conn, error) {
	return sshDial.Client.Dial(network, address)
}

func (sshDial *ViaSSHDialer) DialTimeout(network, address string, timeout time.Duration) (net.Conn, error) {
	return sshDial.Client.Dial(network, address)
}

func (sshDial *ViaSSHDialer) DialWithContext(ctx context.Context, addr string) (net.Conn, error) {
	return sshDial.Client.Dial("tcp", addr)
}
