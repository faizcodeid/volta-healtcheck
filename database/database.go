package database

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh"
	"gorm.io/gorm"
)

var (
	err error

	dbConnections map[string]*gorm.DB
	sshConnection map[string]*ssh.Client
)

func Init() {
	dbConfigurations := map[string]Db{
		strings.ToUpper("postgres"): &dbPostgreSQL{
			db: db{
				Host: os.Getenv("POSTGRES_DB_HOST"),
				User: os.Getenv("POSTGRES_DB_USER"),
				Pass: os.Getenv("POSTGRES_DB_PASS"),
				Port: os.Getenv("POSTGRES_DB_PORT"),
				Name: os.Getenv("POSTGRES_DB_NAME"),
			},
			ssh: sshConf{
				Host: os.Getenv("POSTGRES_SSH_HOST"),
				Port: os.Getenv("POSTGRES_SSH_PORT"),
				User: os.Getenv("POSTGRES_SSH_USER"),
				Pass: os.Getenv("POSTGRES_SSH_PASS"),
			},
			SslMode: os.Getenv("POSTGRES_DB_SSLMODE"),
			Tz:      os.Getenv("POSTGRES_DB_TZ"),
			viaSSH: func(a string) bool {
				b, err := strconv.ParseBool(a)
				if err != nil {
					panic(fmt.Sprintf("Failed to init database POSTGRES, error %v", err))
				}

				return b
			}(os.Getenv("POSTGRES_SSH_TUNNEL")),
		},
		strings.ToUpper("mysql"): &dbMySQL{
			db: db{
				Host: os.Getenv("MYSQL_DB_HOST"),
				User: os.Getenv("MYSQL_DB_USER"),
				Pass: os.Getenv("MYSQL_DB_PASS"),
				Port: os.Getenv("MYSQL_DB_PORT"),
				Name: os.Getenv("MYSQL_DB_NAME"),
			},
			ssh: sshConf{
				Host: os.Getenv("MYSQL_SSH_HOST"),
				Port: os.Getenv("MYSQL_SSH_PORT"),
				User: os.Getenv("MYSQL_SSH_USER"),
				Pass: os.Getenv("MYSQL_SSH_PASS"),
			},
			ParseTime: os.Getenv("MYSQL_DB_PARSE_TIME"),
			viaSSH: func(a string) bool {
				b, err := strconv.ParseBool(a)
				if err != nil {
					panic(fmt.Sprintf("Failed to init database MYSQL, error %v", err))
				}

				return b
			}(os.Getenv("MYSQL_SSH_TUNNEL")),
		},
	}

	dbConnections = make(map[string]*gorm.DB)
	sshConnection = make(map[string]*ssh.Client)
	for k, v := range dbConfigurations {
		if v.ViaSSH() {
			if sshConnection[k], err = v.OpenSSHConnection(); err != nil {
				panic(fmt.Errorf("failed to open ssh connection %s, error %v", k, err))
			}

			logrus.Info(fmt.Sprintf("Successfully connected to ssh %s", k))
		}

		if dbConnections[k], err = v.Init(); err != nil {
			panic(fmt.Sprintf("Failed to connect to database %s", k))
		}

		logrus.Info(fmt.Sprintf("Successfully connected to database %s", k))
	}
}

func Connection(name string) (*gorm.DB, error) {
	if dbConnections[strings.ToUpper(name)] == nil {
		return nil, errors.New("Connection is undefined")
	}
	return dbConnections[strings.ToUpper(name)], nil
}

// Close connection
func Close() {
	var sqlDB *sql.DB

	for key, conn := range dbConnections {
		if sqlDB, err = conn.DB(); err == nil {
			err = sqlDB.Close()
		}
		if err != nil {
			logrus.WithField("message", "failed to close database connection "+key).Error(err.Error())
		} else {
			logrus.Infof("Database connection to %v closed", key)
		}
	}

	for key, conn := range sshConnection {
		if err = conn.Close(); err != nil {
			logrus.WithField("message", "failed to close ssh connection "+key).Error(err.Error())
		} else {
			logrus.Infof("SSH connection to %v closed", key)
		}
	}
}
