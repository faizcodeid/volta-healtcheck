package database

import (
	"database/sql"
	"fmt"
	"time"

	mysqlDriver "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/ssh"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Db interface {
	Init() (*gorm.DB, error)
	ViaSSH() bool
	OpenSSHConnection() (sshConnection *ssh.Client, err error)
}

type db struct {
	Host string
	Port string
	User string
	Pass string
	Name string
}

type sshConf struct {
	Host string
	Port string
	User string
	Pass string
}

type dbPostgreSQL struct {
	db
	ssh     sshConf
	SslMode string
	Tz      string
	viaSSH  bool
}

type dbMySQL struct {
	db
	ssh       sshConf
	ParseTime string
	viaSSH    bool
}

func (c dbPostgreSQL) ViaSSH() bool {
	return c.viaSSH && c.Host != "" && c.Port != "" && c.User != ""
}

func (c *dbPostgreSQL) Init() (*gorm.DB, error) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s", c.Host, c.User, c.Pass, c.Name, c.Port, c.SslMode, c.Tz)

	dialector := postgres.Open(dsn)
	if c.ViaSSH() {
		dialector = postgres.New(postgres.Config{
			DriverName: "postgres+ssh",
			DSN:        dsn,
		})
	}
	db, err := gorm.Open(dialector, &gorm.Config{
		Logger:                 logger.Default.LogMode(logger.Info),
		PrepareStmt:            true,
		SkipDefaultTransaction: true,
	})
	if err != nil {
		return nil, err
	}

	d, err := db.DB()
	if err != nil {
		return nil, err
	}

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	d.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	d.SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	d.SetConnMaxLifetime(10 * time.Minute)

	return db, nil
}

func (c dbPostgreSQL) OpenSSHConnection() (sshConnection *ssh.Client, err error) {
	// The client configuration with configuration option to use the ssh-agent
	sshConfig := &ssh.ClientConfig{
		User:            c.ssh.User,
		Auth:            []ssh.AuthMethod{},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// When there's a non empty password add the password AuthMethod
	sshConfig.Auth = append(sshConfig.Auth, ssh.PasswordCallback(func() (string, error) {
		return c.ssh.Pass, nil
	}))

	// Connect to the SSH Server
	if sshConnection, err = ssh.Dial("tcp", fmt.Sprintf("%s:%s", c.ssh.Host, c.ssh.Port), sshConfig); err == nil {
		// Now we register the ViaSSHDialer with the ssh connection as a parameter
		sql.Register("postgres+ssh", &ViaSSHDialer{Client: sshConnection})
	}
	return
}

func (c dbMySQL) ViaSSH() bool {
	return c.viaSSH && c.Host != "" && c.Port != "" && c.User != ""
}

func (c *dbMySQL) Init() (*gorm.DB, error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=%s", c.User, c.Pass, c.Host, c.Port, c.Name, c.ParseTime)

	if c.ViaSSH() {
		dsn = fmt.Sprintf("%s:%s@mysql+tcp(%s:%s)/%s?parseTime=%s", c.User, c.Pass, c.Host, c.Port, c.Name, c.ParseTime)
	}
	dialector := mysql.Open(dsn)
	db, err := gorm.Open(dialector, &gorm.Config{
		Logger:                 logger.Default.LogMode(logger.Info),
		PrepareStmt:            true,
		SkipDefaultTransaction: true,
	})
	if err != nil {
		return nil, err
	}

	d, err := db.DB()
	if err != nil {
		return nil, err
	}

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	d.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	d.SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	d.SetConnMaxLifetime(10 * time.Minute)

	return db, nil
}

func (c dbMySQL) OpenSSHConnection() (sshConnection *ssh.Client, err error) {
	// The client configuration with configuration option to use the ssh-agent
	sshConfig := &ssh.ClientConfig{
		User:            c.ssh.User,
		Auth:            []ssh.AuthMethod{},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// When there's a non empty password add the password AuthMethod
	sshConfig.Auth = append(sshConfig.Auth, ssh.PasswordCallback(func() (string, error) {
		return c.ssh.Pass, nil
	}))

	// Connect to the SSH Server
	if sshConnection, err = ssh.Dial("tcp", fmt.Sprintf("%s:%s", c.ssh.Host, c.ssh.Port), sshConfig); err == nil {
		// Now we register the ViaSSHDialer with the ssh connection as a parameter
		sshDialer := &ViaSSHDialer{Client: sshConnection}
		mysqlDriver.RegisterDialContext("mysql+tcp", sshDialer.DialWithContext)
	}
	return
}
