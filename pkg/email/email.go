package email

import (
	"bytes"
	"crypto/tls"
	"errors"
	"net"
	"os"
	"strconv"
	"strings"
	"text/template"

	"gopkg.in/gomail.v2"
)

type Request struct {
	body    string
	message *gomail.Message
	dialer  *gomail.Dialer
}

var (
	ErrInvalidEmail  = errors.New("email is invalid")
	ErrInvalidDomain = errors.New("domain not found")
	ErrInvalidPort   = errors.New("port is invalid")
	ErrBodyNotSet    = errors.New("body not set")
)

func NewRequest(to, subject string) (*Request, error) {
	parts := strings.Split(to, "@")
	if len(parts) != 2 {
		return nil, ErrInvalidEmail
	}

	_, err := net.LookupMX(parts[1])
	if err != nil {
		return nil, ErrInvalidDomain
	}

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", os.Getenv("MAIL_SENDER"))
	mailer.SetHeader("To", to)
	mailer.SetHeader("Subject", subject)
	mailer.SetHeader("Important")

	port, err := strconv.Atoi(os.Getenv("MAIL_PORT"))
	if err != nil {
		return nil, ErrInvalidPort
	}
	dialer := gomail.NewDialer(
		os.Getenv("MAIL_HOST"),
		port,
		os.Getenv("MAIL_USERNAME"),
		os.Getenv("MAIL_PASSWORD"),
	)

	dialer.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
	}
	return &Request{
		message: mailer,
		dialer:  dialer,
	}, nil
}

func (r *Request) SendEmail() error {
	if r.body == "" {
		return ErrBodyNotSet
	}

	r.message.SetBody("text/html", r.body)

	go func() {
		r.dialer.DialAndSend(r.message)
	}()
	return nil
}

func (r *Request) SendEmailWithFiles(files ...string) error {
	if r.body == "" {
		return ErrBodyNotSet
	}

	r.message.SetBody("text/html", r.body)

	for _, file := range files {
		r.message.Attach(file)
	}

	go func() {
		r.dialer.DialAndSend(r.message)
	}()

	return nil
}

func (r *Request) ParseFiles(templateFileName string, data any) error {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	if err = t.Execute(&buf, data); err != nil {
		return err
	}
	r.body = buf.String()
	return nil
}

func (r *Request) Parse(tmpl string, data map[string]any) error {
	t, err := template.New("email").Parse(tmpl)
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	if err = t.Execute(&buf, data); err != nil {
		return err
	}

	r.body = buf.String()
	return nil
}

func (r *Request) GetBody() string {
	return r.body
}

func (r *Request) SetBody(body string) {
	r.body = body
}
