package cron

import (
	"time"
	"volta-healthcheck/factory"

	"github.com/go-co-op/gocron"
)

func Init(f *factory.Factory) {
	scheduler := gocron.NewScheduler(time.UTC)
	service := NewService(scheduler)

	service.CronCheckAPIServices()

	// service.CronCheckIDAndCodeOnDBTraccarAndDBSGB(f)

	scheduler.StartAsync()
}
