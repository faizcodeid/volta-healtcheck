package cron

import (
	"volta-healthcheck/app/apis"
	"volta-healthcheck/app/devices"
	"volta-healthcheck/factory"

	"github.com/go-co-op/gocron"
	"github.com/sirupsen/logrus"
)

type Service interface {
	CronCheckAPIServices()
	CronCheckIDAndCodeOnDBTraccarAndDBSGB(f *factory.Factory)
}

type service struct {
	scheduler *gocron.Scheduler
}

func NewService(scheduler *gocron.Scheduler) Service {
	return &service{scheduler}
}

func (s *service) CronCheckAPIServices() {
	logrus.Info("[CRON] (check-api-services): start")

	job, _ := s.scheduler.Every(5).Minute().Do(apis.CronCheckAPIServices)

	job.SingletonMode()
}

func (s *service) CronCheckIDAndCodeOnDBTraccarAndDBSGB(f *factory.Factory) {
	logrus.Info("[CRON] (check-id-and-code-traccar-and-sgb-services): start")

	deviceService := devices.NewService(f)
	job, _ := s.scheduler.Every(10).Minute().Do(deviceService.CronCheckIDAndCodeOnDBTraccarAndDBSGB)

	job.SingletonMode()
}
