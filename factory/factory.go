package factory

import (
	"volta-healthcheck/database"

	"gorm.io/gorm"
)

type Factory struct {
	Mysql      *gorm.DB
	PostgreSql *gorm.DB
}

func NewFactory() *Factory {
	f := &Factory{}
	f.setupDb()

	return f
}

func (f *Factory) setupDb() {
	dbMySQL, err := database.Connection("mysql")
	if err != nil {
		panic("Failed setup db, connection is undefined")
	}

	dbPostgreSQL, err := database.Connection("postgres")
	if err != nil {
		panic("Failed setup db, connection is undefined")
	}

	f.Mysql = dbMySQL
	f.PostgreSql = dbPostgreSQL
}
