package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"volta-healthcheck/database"
	"volta-healthcheck/factory"
	"volta-healthcheck/pkg/cron"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

func init() {
	godotenv.Load("./.env")
}

func main() {
	var (
		PORT   = os.Getenv("APP_PORT")
		server = http.Server{Addr: ":" + PORT}
	)

	database.Init()
	defer database.Close()

	f := factory.NewFactory()

	logrus.Info("HTTP Server is now running")

	cron.Init(f)

	// Start server
	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logrus.Fatal("Failed to start HTTP Server, because: ", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 5 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	signal.Notify(stop, syscall.SIGTERM)
	signal.Notify(stop, syscall.SIGINT)

	//Recieve shutdown signals.
	<-stop

	// fmt.Println("\n\nClosing database connection...")
	// database.Close()

	fmt.Println("\nGracefully shutting down HTTP Server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		logrus.Fatal("Failed to gracefully shutdown HTTP Server, because: ", err)
	}

	fmt.Println("Server stopped")
	fmt.Println("Goodbye!")
	fmt.Println("Exiting")
}
