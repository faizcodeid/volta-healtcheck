package apis

import (
	"net/http"
)

type StackService struct {
	ServiceName string
	PIC         string
	Email       string
	Url         string
	Method      string

	// dynamically based on API response condition
	ResponseStatus string
	ResponseTime   string
	ErrorMessage   string
	ErrorResponse  string
}

var (
	req            *http.Request
	err            error
	responseResult map[string]interface{}
)
