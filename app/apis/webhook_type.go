package apis

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"
)

type (
	WebhookFacts struct {
		Name  string `json:"name"`
		Value string `json:"value"`
	}

	WebhookSection struct {
		ActivityTitle    string         `json:"activityTitle"`
		ActivitySubtitle string         `json:"activitySubtitle"`
		ActivityImage    string         `json:"activityImage"`
		Text             string         `json:"text"`
		Facts            []WebhookFacts `json:"facts"`
	}

	IncomingWebhookMSTeamsRequest struct {
		// for init send API
		url    string `json:"-"`
		method string `json:"-"`

		// payload
		Type       string           `json:"type"`
		Context    string           `json:"context"`
		ThemeColor string           `json:"themeColor"`
		Title      string           `json:"title"`
		Text       string           `json:"text"`
		Summary    string           `json:"summary"`
		Sections   []WebhookSection `json:"sections"`
	}
)

func NewIncomingWebhookMSTeamsRequest(url, method string, ss *StackService) *IncomingWebhookMSTeamsRequest {
	return &IncomingWebhookMSTeamsRequest{
		url:    url,
		method: method,

		Type:       "MessageCard",
		Context:    "https://schema.org/extensions",
		ThemeColor: "4CD137",
		Title:      fmt.Sprintf("[SERVICE ISSUE - %s]: Issue Opened", ss.ServiceName),
		Summary:    "Minta tolong agar segera dicek ya. Terima kasih.",
		Sections: []WebhookSection{
			{
				ActivityTitle:    "Issue Opened",
				ActivitySubtitle: time.Now().Format(time.RFC1123Z),
				ActivityImage:    "https://img.favpng.com/11/10/13/computer-icons-source-code-program-optimization-icon-design-png-favpng-3w6EFp1FCyuc4P0qMy9XW2YJs.jpg",
				Text:             "Minta tolong agar segera dicek ya. Terima kasih.",
				Facts: []WebhookFacts{
					{
						Name:  "Assigned to",
						Value: ss.PIC,
					},
					{
						Name:  "URL",
						Value: ss.Url,
					},
					{
						Name:  "Method",
						Value: ss.Method,
					},
					{
						Name:  "Status",
						Value: ss.ResponseStatus,
					},
					{
						Name:  "Response Time",
						Value: ss.ResponseTime,
					},
					{
						Name:  "Error Message",
						Value: ss.ErrorMessage,
					},
					{
						Name:  "Error Response",
						Value: ss.ErrorResponse,
					},
				},
			},
		},
	}
}

func (r *IncomingWebhookMSTeamsRequest) Url() string {
	return r.url
}

func (r *IncomingWebhookMSTeamsRequest) Method() string {
	return r.method
}

func (r *IncomingWebhookMSTeamsRequest) Body() io.Reader {
	reqBody, _ := json.Marshal(r)

	return strings.NewReader(string(reqBody))
}
