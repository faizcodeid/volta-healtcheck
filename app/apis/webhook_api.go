package apis

import (
	"io"
	"net/http"
)

type IWebhookRequestData interface {
	// required: to call API
	Url() string

	// required: method API call
	Method() string

	// payload request
	Body() io.Reader
}

func SendMSTeamsIncomingWebhook(data IWebhookRequestData) (*http.Response, error) {
	url := data.Url()
	method := data.Method()
	payload := data.Body()
	req, err := http.NewRequest(method, url, payload)
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		return nil, err
	}

	res, err := http.DefaultClient.Do(req)
	return res, err
}
