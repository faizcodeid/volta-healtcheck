package apis

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
	"volta-healthcheck/pkg/email"

	"github.com/sirupsen/logrus"
)

var stackServices = []StackService{
	{
		// Testing
		ServiceName: "testing",
		PIC:         "Faiz",
		Email:       "faiz@code.id",
		Url:         "http://localhost:8000/api/v1/testing",
		Method:      http.MethodGet,
	},
	{
		// API Server Traccar
		ServiceName: "traccar-server",
		PIC:         "Adam",
		Email:       "adam@code.id",
		Url:         "http://sgb.stbkita.com:8082/api/server",
		Method:      http.MethodGet,
	},
	{
		// SGB-Mitra-API-Go
		ServiceName: "sgb-mitra-api-go",
		PIC:         "Faiz",
		Email:       "faiz@code.id",
		Url:         "https://mitra-gapi.stbkita.com/sgb-mitra-api-go/partnerships",
		Method:      http.MethodGet,
	},
	{
		// SGB-API-Go
		ServiceName: "sgb-api-go",
		PIC:         "Mahbub",
		Email:       "mahbub@code.id",
		Url:         "https://gapi.stbkita.com/parameter",
		Method:      http.MethodGet,
	},
	{
		// Volta-API-Go
		ServiceName: "volta-api-go",
		PIC:         "Ali Laode",
		Email:       "laode@code.id",
		Url:         "https://gapi.sgbindonesia.com/parameter",
		Method:      http.MethodGet,
	},
	{
		// SGB-API-NodeJS
		ServiceName: "sgb-api-nodejs",
		PIC:         "Ali Laode",
		Email:       "laode@code.id",
		Url:         "https://napi.stbkita.com",
		Method:      http.MethodGet,
	},
	{
		// Volta-API-NodeJS
		ServiceName: "volta-api-nodejs",
		PIC:         "Ali Laode",
		Email:       "laode@code.id",
		Url:         "https://napi.sgbindonesia.com",
		Method:      http.MethodGet,
	},
}

func CronCheckAPIServices() {
	for _, ss := range stackServices {
		req, err = http.NewRequest(ss.Method, ss.Url, nil)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"cron":    "[CRON] (check-api-services)",
				"message": fmt.Sprintf("[SERVICE] (%s): error when creating HTTP request", ss.ServiceName),
			}).Error(err.Error())
			return
		}

		now := time.Now()
		res, err := http.DefaultClient.Do(req)
		if err != nil || res.StatusCode != http.StatusOK {
			// if http.Response is nil because an error occurred, then the status code is 0
			if res == nil {
				res = &http.Response{
					StatusCode: 0,
					Status:     "0 Unknown",
					Body:       ioutil.NopCloser(strings.NewReader("")),
				}
			}

			if err != nil {
				logrus.WithFields(logrus.Fields{
					"cron":    "[CRON] (check-api-services)",
					"message": fmt.Sprintf("[SERVICE] (%s): error getting source", ss.ServiceName),
				}).Error(err.Error())
			}

			// parsing response body
			resBody, _ := ioutil.ReadAll(res.Body)
			json.Unmarshal(resBody, &responseResult)
			resBody, _ = json.MarshalIndent(responseResult, "", "  ")

			// response status when hitting an API endpoint
			ss.ResponseStatus = res.Status

			// response time when hitting an API endpoint
			ss.ResponseTime = time.Since(now).String()

			// error instance from HTTP Response
			ss.ErrorMessage = fmt.Sprintf("%v", err)

			// JSON response from Server
			ss.ErrorResponse = string(resBody)

			url := os.Getenv("WEBHOOK_MS_TEAMS_URL")
			incomingWebhookMSTeamsReq := NewIncomingWebhookMSTeamsRequest(url, http.MethodPost, &ss)

			go func() {
				if _, err := SendMSTeamsIncomingWebhook(incomingWebhookMSTeamsReq); err != nil {
					logrus.WithFields(logrus.Fields{
						"cron":    "[CRON] (check-api-services)",
						"message": fmt.Sprintf("[SERVICE] (%s): error when sending webhook to MS Teams", ss.ServiceName),
					}).Error(err.Error())
					return
				}
			}()

			e, err := email.NewRequest(ss.Email, incomingWebhookMSTeamsReq.Title)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"cron":    "[CRON] (check-api-services)",
					"message": fmt.Sprintf("[SERVICE] (%s): error when creating new email request", ss.ServiceName),
				}).Error(err.Error())
				return
			}

			bodyEmail := "Dear " + ss.PIC + ",<br><br>berikut terlampir issue dari service " + ss.ServiceName +
				"<br>URL: " + ss.Url + "<br>Method: " + ss.Method + "<br>Status: " + ss.ResponseStatus + "<br>Response Time: " +
				ss.ResponseTime + "<br>Timestamp: " + time.Now().Format(time.RFC1123Z) + "<br>Error Message: " + ss.ErrorMessage + "<br>Error Response: " + ss.ErrorResponse +
				"<br><br>minta tolong agar segera dicek ya. Terima kasih"
			e.SetBody(bodyEmail)

			if err := e.SendEmail(); err != nil {
				logrus.WithFields(logrus.Fields{
					"cron":    "[CRON] (check-api-services)",
					"message": fmt.Sprintf("[SERVICE] (%s): error when sending email issue service", ss.ServiceName),
				}).Error(err.Error())
				return
			}

			ssJson, _ := json.MarshalIndent(ss, "", "	")
			fmt.Println(string(ssJson))
			fmt.Println("[CRON] (check-api-services): Successfully sent webhook to MS Teams for issue of service " + ss.ServiceName)
			fmt.Println()
		}

		res.Body.Close()
	}
}
