package devices

type Device struct {
	ID   uint64 `json:"id"`
	Code string `json:"code"`
}
