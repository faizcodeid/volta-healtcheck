package devices

import (
	"volta-healthcheck/factory"

	"gorm.io/gorm"
)

type DeviceRepository interface {
	GetListBatteryOnTraccar() ([]Device, error)
	GetListBatteryOnDBSGB() ([]Device, error)
}

type devicerepository struct {
	MySQL      *gorm.DB
	PostgreSQL *gorm.DB
}

func NewDeviceRepository(f *factory.Factory) DeviceRepository {
	return &devicerepository{
		MySQL:      f.Mysql,
		PostgreSQL: f.PostgreSql,
	}
}

func (r *devicerepository) GetListBatteryOnTraccar() (datas []Device, err error) {
	err = r.MySQL.Table("tc_devices").Select("id, uniqueid code").Find(&datas).Error
	return
}

func (r *devicerepository) GetListBatteryOnDBSGB() (datas []Device, err error) {
	err = r.PostgreSQL.Table("battery").Select("id, code").Find(&datas).Error
	return
}
