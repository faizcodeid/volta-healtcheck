package devices

import (
	"fmt"
	"volta-healthcheck/factory"

	"github.com/sirupsen/logrus"
)

type service struct {
	DeviceRepository
}

func NewService(f *factory.Factory) *service {
	return &service{
		DeviceRepository: NewDeviceRepository(f),
	}
}

func (s *service) CronCheckIDAndCodeOnDBTraccarAndDBSGB() {
	var (
		err        error
		deviceData []Device
	)

	// Get list battery on traccar
	// Get list battery on dbsgb
	// Compare 2 list
	// If not match, update id and code battery on dbsgb

	// Get list battery on traccar
	deviceData, err = s.DeviceRepository.GetListBatteryOnTraccar()
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"message": "[CRON] (check-id-and-code-traccar-and-sgb-services): error when getting source",
		}).Error(err.Error())
		return
	}

	if len(deviceData) > 0 && err == nil {
		fmt.Println("Data di traccar aman")
	}

	// Get list battery on dbsgb
	deviceData, err = s.DeviceRepository.GetListBatteryOnDBSGB()
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"message": "[CRON] (check-id-and-code-traccar-and-sgb-services): error when getting source",
		}).Error(err.Error())
		return
	}

	if len(deviceData) > 0 && err == nil {
		fmt.Println("Data di db sgb aman")
	}
}
